import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  asideVisible:boolean=false;
  mainAsideContainerDeploy:boolean=true;
 



  constructor() { }

  ngOnInit(): void {

  }  

  asideDeploy(){   
    this.asideVisible=!this.asideVisible; 
    if(window.innerWidth > 700){
      this.mainAsideContainerDeploy=!this.mainAsideContainerDeploy;   
       
    }
  }

}
